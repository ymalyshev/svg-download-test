package sample.android.svgdownloadtest.svg;

import android.content.Context;
import android.graphics.drawable.PictureDrawable;

import androidx.annotation.NonNull;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Registry;
import com.bumptech.glide.annotation.GlideModule;
import com.bumptech.glide.module.AppGlideModule;
import com.caverock.androidsvg.SVG;

import java.io.InputStream;

@GlideModule
public class SvgModule extends AppGlideModule {

    @Override
    public boolean isManifestParsingEnabled() {
        return false;
    }

    @Override
    public void registerComponents(
            @NonNull final Context context,
            @NonNull final Glide glide,
            @NonNull final Registry registry) {
        super.registerComponents(context, glide, registry);
        registry
                .register(SVG.class, PictureDrawable.class, new SvgResourceTranscoder())
                .append(InputStream.class, SVG.class, new SvgDecoder());
    }
}
