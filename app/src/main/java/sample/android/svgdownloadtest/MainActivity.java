package sample.android.svgdownloadtest;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.drawable.PictureDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ImageView imageView = findViewById(R.id.image);
        Glide.with(this)
                .as(PictureDrawable.class)
                .load("https://dl.dropboxusercontent.com/s/r5o1zsnag104z5z/candles.svg")
                .into(imageView);
        /*Glide.with(this)
                .load("https://dl.dropboxusercontent.com/s/7lc8ii29x8xthud/5718897981_10faa45ac3_b-640x624.jpg")
                .into(imageView);*/
    }
}
